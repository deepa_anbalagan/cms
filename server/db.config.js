const mongoose = require("mongoose");

const connectDB = async () => {
  try {
    const conn = await mongoose.connect(
      "mongodb+srv://hashimdb96:hashimdb96@cluster0-yy19d.mongodb.net/Cms?retryWrites=true&w=majority",
      {
        useNewUrlParser: true,
        useCreateIndex: true,
        useUnifiedTopology: true
      }
    );

    console.log(`MongoDB Connected`);
  } catch (err) {
    console.log(`Error`);
    process.exit(1);
  }
};

module.exports = connectDB;
