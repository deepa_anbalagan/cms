const mongoose = require("mongoose");

const SitesSchema = new mongoose.Schema({
  cname: {
    type: String
  },
  country: {
    type: String
  },
  email: {
    type: String
  },
  sname: {
    type: String
  },
  url: {
    type: String
  }
});

module.exports = mongoose.model("Site", SitesSchema);
