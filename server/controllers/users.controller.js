const User = require("../models/users.model");
const mongoose = require("mongoose");

exports.Login = (req, res) => {
  const { email, password } = req.body;
  User.findOne({ email: email, password: password })
    .then(data => {
      if (data != null) {
        res.json({
          success: true,
          message: "successfully logged in",
          data: data
        });
      } else {
        res.json({
          success: false,
          message: "Email or password is incorrect"
        });
      }
    })
    .catch(error =>
      res.json({ success: false, message: "email or password is incorrect" })
    );
};
