const Site = require("../models/sites.model");

exports.AddSites = (req, res) => {
  let data = req.body;
  Site.remove({}, function(err, deleted) {
    if (err) {
      res.json({
        success: false,
        message: "please try again"
      });
    } else {
      Site.insertMany(data, function(err, docs) {
        if (err) {
          res.json({
            success: false,
            data: docs
          });
        } else {
          res.json({
            success: true,
            data: docs
          });
        }
      });
    }
  });
};
