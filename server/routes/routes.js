const express = require("express");
const router = express.Router();
const { Login } = require("../controllers/users.controller");
const { AddSites } = require("../controllers/sites.controller");
router.route("/login").post(Login);
router.route("/addsites").post(AddSites);
module.exports = router;
