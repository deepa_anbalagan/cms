import React from "react";
import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

import SitesTable from "./SitesTable";
const useStyles = makeStyles(theme => ({
  mainContainer: {
    padding: "3rem 6rem",
    [theme.breakpoints.down("md")]: {
      padding: "1rem 1rem"
    }
  }
}));
function MainSection() {
  const classes = useStyles();

  return (
    <>
      <div className={classes.mainContainer}>
        <Typography
          variant="h4"
          color="primary"
          style={{ textAlign: "center", marginBottom: "1em" }}
        >
          Available Sites
        </Typography>
        <SitesTable />
      </div>
    </>
  );
}

export default MainSection;
