import React from "react";
import Header from "./Header";
import MainSection from "./MainSection";
function CMSlanding() {
  return (
    <>
      <Header />
      <MainSection />
    </>
  );
}

export default CMSlanding;
