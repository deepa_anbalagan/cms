/* eslint-disable no-use-before-define */
import React, { useState, useContext } from "react";
import Chip from "@material-ui/core/Chip";
// import Autocomplete from "@material-ui/lab/Autocomplete";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { GlobalContext } from "../context/GlobalState";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
const useStyles = makeStyles(theme => ({
  root: {
    width: 400,
    "& > * + *": {
      marginTop: theme.spacing(3),
      marginRight: theme.spacing(2)
    }
  }
}));

export default function ChooseHeader() {
  const classes = useStyles();
  const [headers, setHeaders] = useState([]);
  const mainContext = useContext(GlobalContext);
  const handleChange = event => {
    mainContext.addHeaders(JSON.parse(event.target.value));
    // console.log(event.target.value);
  };
  // React.useEffect(() => {
  //   console.log(headers);
  // }, [headers]);
  // const getHeaders = header => {
  //   setHeaders(header);
  //   mainContext.addHeaders(header);
  // };
  return (
    <div
      style={{ display: "flex", width: "100%" }}
      className="section_container"
    >
      <Typography
        variant="h6"
        style={{ margin: "1em", textAlign: "center", fontSize: "1rem" }}
        color="primary"
      >
        Choose Header Menu :
      </Typography>

      <TextareaAutosize
        aria-label="minimum height"
        rowsMin={10}
        placeholder="Put Header menu's in JSON format"
        style={{ width: "50%", marginBottom: "1em", fontSize: "1rem" }}
        onChange={e => handleChange(e)}
      />
    </div>
  );
}

// Default Headers
const DefaultHeaders = [
  { title: "Home" },
  { title: "Events" },
  { title: "About Us" },
  { title: "Contact Us" },
  { title: "Sports" },
  { title: "Movies" },
  { title: "Blogs" },
  { title: "Explore" },
  { title: "Shows" },
  { title: "Party" }
];
