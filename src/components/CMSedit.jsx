import React, { useContext } from "react";
import Header from "./Header";
import { makeStyles } from "@material-ui/styles";
import { Paper, Typography, Button } from "@material-ui/core";
import clsx from "clsx";

import CircularProgress from "@material-ui/core/CircularProgress";
import { green } from "@material-ui/core/colors";

import ChooseHeader from "./ChooseHeader";
import ChooseColor from "./ChooseColor";
import ChooseFooter from "./ChooseFooter";
import ChooseFont from "./ChooseFont";
import ChooseIcon from "./ChooseIcon";
import ChooseGA from "./ChooseGA";
import CloudDownload from "@material-ui/icons/CloudDownload";
import { GlobalContext } from "../context/GlobalState";

const useStyles = makeStyles(theme => ({
  mainContainer: {
    margin: "0 auto",
    width: "90vw",
    height: "auto",
    padding: "0 2em"
  },
  wrapper: {
    padding: "1em",
    margin: "0 20%",
    width: "auto"
  },
  imgContainer: {
    margin: "0 auto"
  },
  image: {
    width: "50rem",
    height: "40rem",
    display: "block",
    margin: "auto"
  },
  info: {
    fontSize: "2rem",
    textAlign: "center",
    marginTop: "20px",
    marginBottom: "20px"
  },
  editContainer: {
    display: "flex",
    margin: "0 auto",
    flexDirection: "column",
    flexWrap: "wrap",
    alignItems: "flex-start"
  },
  buttonSuccess: {
    backgroundColor: green[500],
    "&:hover": {
      backgroundColor: green[700]
    }
  },
  buttonProgress: {
    color: green[500],
    position: "absolute",
    top: "50%",
    left: "50%",
    marginTop: -12,
    marginLeft: -12
  }
}));
const header_color = ["#6b5b95", "#d64161"];
const footer_color = ["#6b5b95", "#d64161"];
const color_primary = ["#6b5b95", "#d64161"];
const color_secondary = ["#6b5b95", "#d64161"];
function CMSedit() {
  const classes = useStyles();
  const mainContext = useContext(GlobalContext);
  const [loading, setLoading] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const timer = React.useRef();

  const buttonClassname = clsx({
    [classes.buttonSuccess]: success
  });

  React.useEffect(() => {
    return () => {
      clearTimeout(timer.current);
    };
  }, []);

  const handleButtonClick = () => {
    if (!loading) {
      setSuccess(false);
      setLoading(true);
      timer.current = setTimeout(() => {
        setSuccess(true);
        setLoading(false);
        mainContext.Submit();
      }, 2000);
    }
  };
  return (
    <>
      <Header />
      <Paper elevation={3} className={classes.mainContainer}>
        <div className="wrapper">
          <Typography variant="h5" color="primary" className={classes.info}>
            Sistic Booking Engine
          </Typography>
          <div className={classes.editContainer}>
            <div className="colors-container">
              <div className="colors-wrapper">
                <div className="colors-info">Header Background color :</div>
                <ChooseColor type="headerbr" hexa="#d3cfcc" />
              </div>
              <div className="colors-wrapper">
                <div className="colors-info">Footer Background color :</div>
                <ChooseColor type="footerbg" hexa="#7c7979" />
              </div>
            </div>
            <div className="colors-container">
              <div className="colors-wrapper">
                <div className="colors-info">Header Font color :</div>
                <ChooseColor type="headerFontColor" hexa="#333" />
              </div>
              <div className="colors-wrapper">
                <div className="colors-info">Footer Font color :</div>
                <ChooseColor type="footerFontColor" hexa="#333" />
              </div>
            </div>
            <div className="colors-container">
              <div className="colors-wrapper">
                <div className="colors-info">Primary color :</div>
                <ChooseColor type="primary" hexa="#f37011" />
              </div>
              <div className="colors-wrapper">
                <div className="colors-info">Secondary color :</div>
                <ChooseColor type="secondary" hexa="#5925c7" />
              </div>
            </div>
            <ChooseHeader />
            <ChooseFooter />
            <ChooseGA />
            <div className={classes.wrapper}>
              <Button
                variant="contained"
                color="primary"
                className={buttonClassname}
                disabled={loading}
                onClick={handleButtonClick}
              >
                Submit
              </Button>
              {loading && (
                <CircularProgress
                  size={24}
                  className={classes.buttonProgress}
                />
              )}
              <Button
                variant="contained"
                color="primary"
                className={classes.button}
                startIcon={<CloudDownload />}
                style={{ margin: "1em" }}
                disabled={mainContext.download}
                onClick={mainContext.Download}
              >
                Download Theme
              </Button>
            </div>
          </div>
        </div>
      </Paper>
    </>
  );
}

export default CMSedit;
