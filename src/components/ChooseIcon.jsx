import React, { useState, useEffect, useContext } from "react";
import { DropzoneArea } from "material-ui-dropzone";
// import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { GlobalContext } from "../context/GlobalState";

const ChooseIcon = () => {
  const [file, setFile] = useState([]);
  const mainContext = useContext(GlobalContext);
  const handleChange = files => {
    setFile(files[0]);
    mainContext.addLogo(files[0]);
  };
  useEffect(() => {
    console.log(file);
  }, [file]);
  return (
    <>
      <div style={{ display: "flex" }} className="section_container">
        <Typography
          variant="h6"
          style={{ margin: "1em", textAlign: "center", fontSize: "1rem" }}
          color="secondary"
        >
          Upload logo or icon :
        </Typography>
        <DropzoneArea
          onChange={handleChange}
          dropzoneClass="dropZone_custom"
          showPreviews={true}
          showPreviewsInDropzone={false}
        />
      </div>
    </>
  );
};

export default ChooseIcon;
