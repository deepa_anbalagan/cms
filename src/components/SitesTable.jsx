import React, { useEffect, useContext } from "react";
import MaterialTable from "material-table";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { GlobalContext } from "../context/GlobalState";
export default function SitesTable() {
  const [state, setState] = React.useState({
    columns: [
      { title: "Site Name", field: "sname" },
      {
        title: "Site URL",
        field: "url",
        render: rowData => <a href={rowData.url}>{rowData.url}</a>
      },
      { title: "Customer Name", field: "cname" },
      {
        title: "Country",
        field: "conuntry"
      },
      {
        title: "Email",
        field: "email"
      },
      {
        title: "Edit Site",
        filed: "edit",
        render: rowData => (
          <Button variant="contained" color="primary">
            <Link
              to={`/edit`}
              style={{ color: "white", textDecoration: "none" }}
            >
              {" "}
              Edit{" "}
            </Link>
          </Button>
        )
      }
    ],
    data: [
      {
        sname: "Booking Engine",
        url: "www.bookingengine.com",
        cname: "ABC Limited",
        conuntry: "India",
        email: "abc@email.com"
      },
      {
        sname: "Travel Website",
        url: "www.mytrip.com",
        cname: "Tech Soft",
        conuntry: "Singapore",
        email: "tech@email.com"
      },
      {
        sname: "Travel Website",
        url: "www.mytrip.com",
        cname: "Tech Soft",
        conuntry: "Singapore",
        email: "tech@email.com"
      },
      {
        sname: "Travel Website",
        url: "www.mytrip.com",
        cname: "Tech Soft",
        conuntry: "Singapore",
        email: "tech@email.com"
      },
      {
        sname: "Travel Website",
        url: "www.mytrip.com",
        cname: "Tech Soft",
        conuntry: "Singapore",
        email: "tech@email.com"
      },
      {
        sname: "Travel Website",
        url: "www.mytrip.com",
        cname: "Tech Soft",
        conuntry: "Singapore",
        email: "tech@email.com"
      }
    ]
  });
  const mainContext = useContext(GlobalContext);
  useEffect(() => {
    mainContext.AddSites(state.data);
  }, [state]);
  return (
    <MaterialTable
      title="Add Site"
      columns={state.columns}
      data={state.data}
      options={{
        actionsColumnIndex: -1,
        paging: true,
        addRowPosition: "first",
        search: false,
        grouping: false
      }}
      editable={{
        onRowAdd: newData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.push(newData);
                return { ...prevState, data };
              });
            }, 600);
          }),
        onRowUpdate: (newData, oldData) =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              if (oldData) {
                setState(prevState => {
                  const data = [...prevState.data];
                  data[data.indexOf(oldData)] = newData;
                  return { ...prevState, data };
                });
              }
            }, 600);
          }),
        onRowDelete: oldData =>
          new Promise(resolve => {
            setTimeout(() => {
              resolve();
              setState(prevState => {
                const data = [...prevState.data];
                data.splice(data.indexOf(oldData), 1);
                return { ...prevState, data };
              });
            }, 600);
          })
      }}
    />
  );
}
