/* eslint-disable no-use-before-define */
import React, { useState, useContext } from "react";
import Chip from "@material-ui/core/Chip";
// import Autocomplete from "@material-ui/lab/Autocomplete";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { GlobalContext } from "../context/GlobalState";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
const useStyles = makeStyles(theme => ({
  root: {
    width: 450,
    "& > * + *": {
      marginTop: theme.spacing(3),
      marginLeft: theme.spacing(2)
    }
  }
}));

export default function ChooseFooter() {
  const classes = useStyles();
  const [footers, setFooters] = useState([]);
  const mainContext = useContext(GlobalContext);
  React.useEffect(() => {
    console.log(footers);
  }, [footers]);
  const handleChange = event => {
    mainContext.addFooters(JSON.parse(event.target.value));
    // console.log(event.target.value);
  };

  return (
    <div
      style={{ display: "flex", width: "100%" }}
      className="section_container"
    >
      <Typography
        variant="h6"
        style={{ margin: "1em", textAlign: "center", fontSize: "1rem" }}
        color="primary"
      >
        Choose Footer Menu :
      </Typography>

      <TextareaAutosize
        aria-label="minimum height"
        rowsMin={10}
        placeholder="Put footer menu's in JSON Format"
        style={{ width: "50%", marginBottom: "1em", fontSize: "1rem" }}
        onChange={e => handleChange(e)}
      />
    </div>
  );
}

// Default Headers
const DefaultFooter = [
  { title: "Home" },
  { title: "Events" },
  { title: "Careers" },
  { title: "Contact Us" },
  { title: "Sports" },
  { title: "Movies" },
  { title: "Blogs" },
  { title: "Explore" },
  { title: "Shows" },
  { title: "Party" }
];
