import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import { Typography, Paper, Button } from "@material-ui/core";
const useStyles = makeStyles(theme => ({
  mainContainer: {
    marginBottom: "2em"
  },
  cardContainer: {
    display: "flex",
    padding: "2em",
    marginTop: "1em"
  },
  cardImage: {
    width: "300px",
    height: "300px"
  },
  cardHeading: {
    fontSize: "2rem"
  },
  cardInfo: {
    marginLeft: "1em"
  },
  cardSecondary: {
    fontSize: "1.1rem",
    color: "#b0bec5",
    marginBottom: "15px"
  }
}));
function SitesCard(props) {
  const classes = useStyles();
  return (
    <Paper elevate={3} className={classes.mainContainer}>
      <div className={classes.cardContainer}>
        <div>
          <img src={props.img} alt="siteimage" className={classes.cardImage} />
        </div>
        <div className={classes.cardInfo}>
          <Typography
            variant="h6"
            className={classes.cardHeading}
            color="primary"
          >
            Sistic Booking Engine
          </Typography>
          <div className={classes.cardSecondary}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem
            <div style={{ marginTop: "10px" }}>
              {" "}
              Ipsum. when an unknown printer took a galley of type and scrambled
              it to make a type specimen book. It has survived not only five
              centuries, but also the leap into electronic typesetting,
              remaining essentially unchanged.
            </div>
          </div>
          <Button variant="contained" color="primary">
            {" "}
            <Link
              to="/site/edit"
              style={{ textDecoration: "none", color: "#fff" }}
            >
              {" "}
              EDIT
            </Link>
          </Button>
        </div>
      </div>
    </Paper>
  );
}

export default SitesCard;
