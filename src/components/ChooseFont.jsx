import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { GlobalContext } from "../context/GlobalState";
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    display: "flex"
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));
function ChooseFont() {
  const classes = useStyles();
  const [font, setFont] = React.useState("");
  const mainContext = useContext(GlobalContext);
  //fonts

  const font1 = {
    fontFamily: "Open Sans",
    url:
      "url('https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap')"
  };
  const font2 = {
    fontFamily: "Inconsolata",
    url:
      "url('https://fonts.googleapis.com/css2?family=Inconsolata:wght@200;300;400;500;577;600;700;800;900&display=swap')"
  };
  const font3 = {
    fontFamily: "Dancing Script",
    url:
      "url('https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600&display=swap')"
  };
  const handleChange = event => {
    setFont(event.target.value);
    let ftype = event.target.value;
    switch (ftype) {
      case 1:
        return mainContext.addFont(font1);
      case 2:
        return mainContext.addFont(font2);
      case 3:
        return mainContext.addFont(font3);

      default:
        return mainContext.addFont(font1);
    }
  };
  return (
    <>
      <div style={{ display: "flex" }}>
        <Typography
          variant="h6"
          style={{ margin: "1em", textAlign: "center", fontSize: "1rem" }}
          color="secondary"
        >
          Choose Font :
        </Typography>
        <FormControl variant="outlined" className={classes.formControl}>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={font}
            onChange={handleChange}
          >
            <MenuItem value={1}>
              <div className="font-1">The Responsive Booking Engine</div>
            </MenuItem>
            <MenuItem value={2}>
              <div className="font-2">The Responsive Booking Engine</div>
            </MenuItem>
            <MenuItem value={3}>
              <div className="font-3">The Responsive Booking Engine</div>
            </MenuItem>
          </Select>
        </FormControl>
      </div>
    </>
  );
}

export default ChooseFont;
