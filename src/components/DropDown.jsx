import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import { GlobalContext } from "../context/GlobalState";
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));

export default function DropDown(props) {
  const classes = useStyles();
  const [color, setColor] = React.useState("");
  const mainContext = useContext(GlobalContext);
  const handleChange = event => {
    setColor(event.target.value);
    let value = event.target.value;
    console.log(value, "value");
    mainContext.addColors(props.color, value);
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <Typography
        variant="h6"
        style={{ margin: "1em 0", textAlign: "center" }}
        color="secondary"
      >
        {props.color} Color
      </Typography>

      <Select
        labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-outlined"
        value={color}
        onChange={handleChange}
        displayEmpty
      >
        <MenuItem value="">
          <div
            style={{
              backgroundColor: props.default,
              color: "white",
              fontSize: "1.5rem",
              padding: "10px"
            }}
          >
            {props.default}
          </div>
        </MenuItem>
        {props.data.map((item, index) => {
          return (
            <MenuItem value={item}>
              <div
                style={{
                  backgroundColor: item,
                  color: "white",
                  fontSize: "1.5rem",
                  padding: "10px"
                }}
              >
                {item}
              </div>
            </MenuItem>
          );
        })}
      </Select>
    </FormControl>
  );
}
