import React, { useContext } from "react";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import Typography from "@material-ui/core/Typography";
import FormControl from "@material-ui/core/FormControl";
import { makeStyles } from "@material-ui/core/styles";
import { GlobalContext } from "../context/GlobalState";
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 220
  },
  selectEmpty: {
    marginTop: theme.spacing(2)
  }
}));
const ChooseGA = () => {
  const classes = useStyles();
  const mainContext = useContext(GlobalContext);
  const handleChange = event => {
    mainContext.addGA(event.target.value);
    // console.log(event.target.value);
  };
  return (
    <>
      <div
        style={{ display: "flex", width: "100%", marginTop: "1em" }}
        className="section_container"
      >
        <Typography
          variant="h6"
          style={{ margin: "2em", textAlign: "center", fontSize: "1rem" }}
          color="primary"
        >
          Google Analytics :
        </Typography>

        <TextareaAutosize
          aria-label="minimum height"
          rowsMin={10}
          placeholder="Google Analytics"
          style={{ width: "50%", marginBottom: "1em", fontSize: "1rem" }}
          onChange={e => handleChange(e)}
        />
      </div>
    </>
  );
};
export default ChooseGA;
