import React from "react";
import reactCSS from "reactcss";
import { SketchPicker } from "react-color";
import { GlobalContext } from "../context/GlobalState";
class ChooseColor extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayColorPicker: false,
      color: {
        r: "241",
        g: "112",
        b: "19",
        a: "1"
      },
      hexa: "#e76c14"
    };
  }
  static contextType = GlobalContext;

  componentDidMount() {
    this.setState({ hexa: this.props.hexa });
    const context = this.context;
  }

  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker });
  };

  handleClose = () => {
    this.setState({ displayColorPicker: false });
  };

  handleChange = color => {
    this.setState({ color: color.rgb });
    this.setState({ hexa: color.hex });
    this.context.addColors(this.props.type, color.hex);
  };

  render() {
    const styles = reactCSS({
      default: {
        color: {
          width: "100px",
          height: "50px",
          borderRadius: "2px",
          color: "#fff",
          textAlign: "center",
          padding: "15px 10px 0px 10px",
          fontSize: "1rem",
          //   background: `rgba(${this.state.color.r}, ${this.state.color.g}, ${this.state.color.b}, ${this.state.color.a})`
          background: this.state.hexa
        },
        swatch: {
          padding: "5px",
          background: "#fff",
          borderRadius: "1px",
          boxShadow: "0 0 0 1px rgba(0,0,0,.1)",
          display: "inline-block",
          cursor: "pointer"
        },
        popover: {
          position: "absolute",
          zIndex: "2"
        },
        cover: {
          position: "fixed",
          top: "0px",
          right: "0px",
          bottom: "0px",
          left: "0px"
        }
      }
    });

    return (
      <div>
        <div style={styles.swatch} onClick={this.handleClick}>
          <div style={styles.color}>{this.state.hexa}</div>
        </div>
        {this.state.displayColorPicker ? (
          <div style={styles.popover}>
            <div style={styles.cover} onClick={this.handleClose} />
            <SketchPicker
              color={this.state.color}
              onChange={this.handleChange}
            />
          </div>
        ) : null}
      </div>
    );
  }
}

export default ChooseColor;
