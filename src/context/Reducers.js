export default (state, action) => {
  switch (action.type) {
    case "LOGIN": {
      return {
        ...state,
        loggedIn: action.payload
      };
    }

    case "ADD_COLORS": {
      let newState = state.colors.filter(item => {
        return Object.keys(item).join() !== action.payload.section;
      });
      return {
        ...state,
        colors: [
          ...newState,
          { [action.payload.section]: action.payload.colors }
        ]
      };
    }

    case "ADD_FONT":
      return {
        ...state,
        font: action.payload
      };
    case "ADD_HEADERS":
      return {
        ...state,
        headers: action.payload
      };
    case "ADD_FOOTERS":
      return {
        ...state,
        footers: action.payload
      };
    case "ADD_LOGO":
      return {
        ...state,
        logo: [...state.logo, action.payload]
      };
    case "ADD_GA": {
      return {
        ...state,
        googleAnalytics: action.payload
      };
    }
    case "DOWNLOAD":
      return {
        ...state,
        download: action.payload
      };
    default:
      return state;
  }
};
