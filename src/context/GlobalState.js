import React, { createContext, useReducer } from "react";
import AppReducer from "./Reducers";
import axios from "axios";
const initialState = {
  font: {},
  colors: [],
  headers: [],
  footers: [],
  logo: [],
  googleAnalytics: "",
  download: true,
  loggedIn: false
};

// Create context
export const GlobalContext = createContext(initialState);
// let url;
// Provider component
export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, initialState);

  //actions

  async function LogIn(email, password) {
    try {
      await axios
        .post("http://localhost:5000/api/v1/auth/login", { email, password })
        .then(data => {
          console.log(data);

          if (data.data.success === true) {
            dispatch({ type: "LOGIN", payload: true });
            console.log(state);
          } else {
            dispatch({ type: "LOGIN", payload: false });
          }
        });
    } catch (error) {
      console.log(error);
    }
  }
  async function AddSites(data) {
    try {
      await axios
        .post("http://localhost:5000/api/v1/auth/addsites", data)
        .then(data => {
          console.log(data);
        });
    } catch (error) {
      console.log(error);
    }
  }

  function addColors(section, colors) {
    dispatch({ type: "ADD_COLORS", payload: { section, colors } });
  }

  function addFont(fonts) {
    dispatch({ type: "ADD_FONT", payload: fonts });
  }

  function addHeaders(headers) {
    dispatch({ type: "ADD_HEADERS", payload: headers });
  }
  function addFooters(footers) {
    dispatch({ type: "ADD_FOOTERS", payload: footers });
  }

  function addLogo(logo) {
    dispatch({ type: "ADD_LOGO", payload: logo });
  }

  function addGA(text) {
    console.log(text);
    dispatch({ type: "ADD_GA", payload: text });
  }
  async function Submit() {
    console.log("submitted", state);
    try {
      //   await axios
      //     .post("http://localhost:5000/configRouter", {  })
      //     .then(function(res) {
      //       url = res.data;

      //     });
      // } catch (error) {
      //   console.log(error);
      setTimeout(() => {
        dispatch({ type: "DOWNLOAD", payload: false });
      }, 2000);
    } catch (error) {
      console.log(error);
    }
  }
  async function Download() {
    try {
      // var a = document.createElement("a");
      // a.href = url;
      // a.setAttribute("download", "env.js");
      // a.click();
      var fileDownload = require("js-file-download");
      fileDownload(JSON.stringify(state), "env.js");
    } catch (error) {
      console.log(error);
    }
  }
  return (
    <GlobalContext.Provider
      value={{
        addColors,
        addFont,
        addFooters,
        addHeaders,
        addLogo,
        Submit,
        Download,
        addGA,
        LogIn,
        AddSites,
        loggedIn: state.loggedIn,
        colors: state.colors,
        download: state.download
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
