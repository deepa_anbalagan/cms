import React from "react";
import CMSlanding from "./components/CMSlanding";
import CMSedit from "./components/CMSedit";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { GlobalProvider } from "./context/GlobalState";
import Login from "./components/Login";
function App() {
  return (
    <>
      <GlobalProvider>
        <Router>
          <Route exact path="/" component={CMSlanding} />
          <Route exact path="/edit" component={CMSedit} />
          <Route exact path="/login" component={Login} />
        </Router>
      </GlobalProvider>
    </>
  );
}

export default App;
